package rom99;

import java.awt.Rectangle;

/**
 * Contains area data model, including path connection points (North, East, South, West)
 * and visibility
 * 
 * @author rossmills
 * @date 15/05/2012
 */
public abstract class Area {
	protected int mapCol, mapRow;
	protected boolean visible = false;
	protected boolean isEnd = false;
	protected boolean isStart = false;
	protected boolean[] connects = new boolean[4];
	protected Rectangle bounds;
	protected int rotation = 0;
	protected int state;
	
	//constants for connects
    public static final int NORTH = 0;
    public static final int EAST = 1;
    public static final int SOUTH = 2;
    public static final int WEST = 3;   
    
    //constants for state
    public static final int JUNGLE = 0;
    public static final int VILLAGE = 1;
    public static final int BURNED = 2;
    public static final int DESERT = 3;
    
    public Rectangle getBounds() {
    	return bounds;
    }
    
    public void setBounds(Rectangle rect) {
    	bounds = rect;
    }
    
    public void rotate() {
        rotation = (rotation + 1) % 4;
        boolean spare = connects[NORTH];
        connects[NORTH] = connects[WEST];
        connects[WEST] = connects[SOUTH];
        connects[SOUTH] = connects[EAST];
        connects[EAST] = spare;
    }
    
    abstract public String getPathType();
    
    public String getAreaType() {
    	return "Jungle";
    }
    
    /**
     * Create a new area with no connections
     */
    public Area() {
    	// do nothing
    }
    
    /**
     * Create a new area with a connection at i
     * @param i
     */
    public Area(int connectionDirection) {
    	connects[connectionDirection] = true;
    }
    
	/**
	 * @return the mapCol
	 */
	public int getMapCol() {
		return mapCol;
	}
	/**
	 * @param mapCol the mapCol to set
	 */
	public void setMapCol(int mapCol) {
		this.mapCol = mapCol;
	}
	/**
	 * @return the mapRow
	 */
	public int getMapRow() {
		return mapRow;
	}
	/**
	 * @param mapRow the mapRow to set
	 */
	public void setMapRow(int mapRow) {
		this.mapRow = mapRow;
	}
	/**
	 * @return whether the tile connects at this point (use Area constants NORTH, EAST, SOUTH or WEST)
	 */
	public boolean getConnect(int i) {
		return connects[i];
	}
	
	/**
	 * Toggles the tile's connection at this point (use Area constants NORTH, EAST, SOUTH or WEST)
	 */
	public void setConnect(int i, boolean b) {
		connects[i] = b;
	}
	
	/**
	 * @return True if this tile should be visible
	 */
	public boolean isVisible() {
		return visible;
	}
	/**
	 * @param Set true if this tile should be visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	/**
	 * @return true if this is the end tile
	 */
	public boolean isEnd() {
		return isEnd;
	}
	/**
	 * @param set to true if this is the end tile
	 */
	public void setEnd(boolean isEnd) {
		this.isEnd = isEnd;
	}

	/**
	 * @return the isStart
	 */
	public boolean isStart() {
		return isStart;
	}

	/**
	 * @param isStart the isStart to set
	 */
	public void setStart(boolean isStart) {
		this.isStart = isStart;
	}

	/**
	 * @return the rotation
	 */
	public int getRotation() {
		return rotation;
	}

	/**
	 * @param rotation the rotation to set
	 */
	public void setRotation(int rotation) {
		this.rotation = rotation;
	}

	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}   
}
