package rom99;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * Contains image details for a particular area
 * @author rossmills
 * @date 16/05/2012
 */

public class AreaView {
	private BufferedImage image;
	private BufferedImage notVisibleImage; 
	ImageLoader imageLoader;
	
	Area area;
	
	/**
	 * AreaView must have a type and a rotation value
	 * @param type
	 * @param rotation
	 */
	public AreaView(ImageLoader tileLoader, String type, int rotation, Area area) {		
		this.area = area;
		imageLoader = tileLoader;

		this.setNotVisibleImage("ThickJungle0");		
		this.setImage(type, rotation);
	}
	
	/**
	 * Draw the AreaView's image at the specified location
	 * @param locX
	 * @param locY
	 * @param width
	 * @param height
	 * @param g
	 */
	public void draw(int locX, int locY, int width, int height, Graphics g) {
		if(area.visible){ 
			g.drawImage(image, locX, locY, width, height, null);
		} else {
			g.drawImage(notVisibleImage, locX, locY, width, height, null);
		}
		if(image == null) {
			g.fillOval(locX, locY, width, height);
		}
	}
	
	/**
	 * Set the image to be displayed when this tile is not visible (i.e. has not been explored)
	 * Resources should be in the src/res folder
	 * 
	 * @param name The resource name (e.g. "ThickJungle0.png")
	 */
	public void setNotVisibleImage(String imageName) {
		notVisibleImage = imageLoader.getImage(imageName);
		if(notVisibleImage == null ) {
			System.out.println(imageName + " = null");
		}
	}
	
	/**
	 * Change the AreaView's image
	 * @param type
	 * @param rotation
	 */
	public void setImage(String type, int rotation) {
		image = imageLoader.getImage(type+rotation);		
	}
}
