package rom99;

import java.awt.Point;

public class Bullet extends Sprite {
	
	private int speed = 5;

	public Bullet(int locX, int locY) {
		this.locX = locX;
		this.locY = locY;
	}
	
	@Override
	public String getName() {
		return "noImage";
	}

	@Override
	public int getWidth() {
		return 4;
	}

	@Override
	public int getHeight() {
		return 9;
	}
	

}
