package rom99;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class BulletView extends SpriteView {
	
	public BulletView(Sprite sprite) {
		this.sprite = sprite;
		int height = sprite.getHeight();
		int width = sprite.getWidth();
		if(sprite.getDirection() == 1 || sprite.getDirection() == 3) {
			width = sprite.getHeight();
			height = sprite.getWidth();
		}
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB );
		Graphics g = image.getGraphics();
		g.setColor(Color.BLACK);
		g.fillRoundRect(0, 0, width, height, 3, 3);
		g.dispose();
	}
	
	@Override
	public void draw(Graphics g) {
		g.drawImage(image, sprite.getLocX(), sprite.getLocY(), null);	
	}	

	@Override
	public Dimension getSpriteSize() {
		return new Dimension(sprite.getWidth(), sprite.getHeight() );
	}

}
