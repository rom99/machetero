package rom99;

public class CornerPath extends Area {
	
	public CornerPath() {
		connects[NORTH] = true;
		connects[EAST] = true;
		connects[SOUTH] = false;
		connects[WEST] = false;
	}

	@Override
	public String getPathType() {
		return "Corner";
	}
}
