package rom99;

public class CrossPath extends Area {
	
	public CrossPath() {
		connects[NORTH] = true;
		connects[EAST] = true;
		connects[SOUTH] = true;
		connects[WEST] = true;
	}

	@Override
	public String getPathType() {
		return "Cross";
	}
	
	@Override
	public int getRotation() {
		return rotation % 2;
	}
}
