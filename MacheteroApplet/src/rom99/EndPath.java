package rom99;

public class EndPath extends Area {

	public EndPath() {
		connects[NORTH] = true;
		connects[EAST] = false;
		connects[SOUTH] = false;
		connects[WEST] = false;
	}

	@Override
	public String getPathType() {
		return "Terminus";
	}
}
