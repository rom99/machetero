package rom99;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.Timer;

/**
 * Main game controlling class, includes the animation thread.
 * 
 * @author rossmills
 *
 */
public class GameController implements Runnable {
	private static GameView view;
	private static GameModel model;
	private static final int DELAY = 18;
	private static final int NO_DELAYS_PER_YIELD = 16;
	private static final int MAX_FRAME_SKIPS = 3;
	public static boolean STATS_ON = false;
	public static boolean DISPLAY_STATUS = true;
	
	private static ImageLoader tileImages = new ImageLoader("JungleTiles/img_def.txt");
	private static ImageLoader spriteImages = new ImageLoader("sprites/img_def.txt");
	private static ImageLoader macheteroImages = new ImageLoader("MacheteroNormal/img_def.txt");

    long period = DELAY * 1000000L; // ms -> nano
	
	private static int numRows = 8;
	private static int numCols = 8;
	
	private Thread animator;
	
	private static volatile boolean running;
	
	private static volatile int moveX, moveY, direction;	
	
	/**
	 *  Animator thread - repeatedly update, render to buffer and repaint
	 */
	@Override
	public void run() {
		long beforeTime, afterTime, timeDiff, sleepTime; 
		long overSleepTime = 0L;
		long excess = 0L;
		
		int noDelays = 0;
				
		System.out.println("\nAnimator thread running...");			
		beforeTime = System.nanoTime();
		threadStartTime = System.nanoTime();
		System.out.println("ThreadStartTime: " + threadStartTime );
		
		running = true;
		while(running) {
			if(!model.isGamePaused() ) {
				gameUpdate(); // game state is updated
			}
			view.gameRender(); // render to a buffer image
			view.paintScreen(); // paint the buffer image to the screen
			
			// for stats gathering
			updates++;
			frames++;
			
			afterTime = System.nanoTime();			
			timeDiff = afterTime - beforeTime;
			sleepTime = (period - timeDiff) - overSleepTime;
			if (sleepTime > 0) {   // some time left in this cycle
				try {
					Thread.sleep(sleepTime/1000000L); // nano -> ms }
				}
				catch(InterruptedException ex){
					System.err.println("Animator thread interrupted");
				}
				overSleepTime = (System.nanoTime() - afterTime) - sleepTime;
			}
			else {    // sleepTime <= 0; frame took longer than the period
				excess -= sleepTime;  // store excess time value
				overSleepTime = 0L;
				if (++noDelays >= NO_DELAYS_PER_YIELD) {
					Thread.yield(); // give another thread a chance to run 
					noDelays = 0;
				}
			}
			beforeTime = System.nanoTime();
			/* If frame animation is taking too long, update the game state
		     without rendering it, to get the updates/sec nearer to
		     the required FPS. */
			int skips = 0;
			while((excess > period) && (skips < MAX_FRAME_SKIPS)) {
				excess -= period;
				gameUpdate(); 
				skips++;
				
				// for stats gathering
				updates++;
				skippedFrames++;
			}
			stats();
		  }
		System.out.println("Animator thread stopped");
			//System.exit(0); // so enclosing JFrame/JApplet exits 
	} // end of run()
	
	/* Used for stats gathering */
	private long threadStartTime = 0;
	private int frames = 0;
	private int updates = 0;
	public static int skippedFrames = 0;
	public static int FPS = 0;
	public static int UPS = 0;
	
	/* Records stats */
	private void stats() {
		long now = System.nanoTime();
		// System.out.println("Now: " + now);
		long threadRunTime = now - threadStartTime;
		// System.out.println("Thread run time: " + threadRunTime);
		if ( threadRunTime > 1000000000L) {
			FPS = frames / (int) ( threadRunTime / 1000000000L ); // Nano -> seconds
			UPS = updates / (int) ( threadRunTime / 1000000000L ); // Nano -> seconds			
		}
		// System.out.printf("FPS: %d\tUPS: %d\n", FPS, UPS);
	}
	
	/**
	 * Set the GameView for this GameController
	 */
	public void setGameView(GameView view) {
		this.view = view;
	}
	
	/**
	 * Sets the number of rows/cols for the map
	 */
	public void setMapSize(int size) {
		System.out.printf("mapSize=%d\n", size);
		numRows = size;
		numCols = size;
	}
	
	/**
	 * Start a new game with a new map and a new Machetero (new model and new view)
	 */	
	public void newGame(GameListener listener) {
		MapFactory factory = new MapFactory();
		GameMap newMap = factory.getRandomMap(numRows, numCols);
		
		SpriteFactory sprFact = new SpriteFactory(newMap);
		ArrayList<Sprite> sprites = sprFact.getRandomSprites();
		
		Machetero machetero = new Machetero (newMap );
		
		model = new GameModel( machetero, newMap, sprites );
		model.addGameListener(listener);
		
		view = new GameView(model, tileImages, spriteImages, macheteroImages);
	}
	
	/**
	 * Update all the game data - called from animator thread
	 */
	private void gameUpdate() {
		
		if(model.isGameWon() ) {
			gameWon();
		} else if(!model.getMachetero().isAlive() ) {
			gameOver();
		} else {
			model.updateMachetero(moveX, moveY, direction);
			model.updateSprites();
		}
	}
	
	/**
	 *  End the animation thread
	 */
	public void endGame() {
		if(running) {
			running = false;
			try {
				// Wait for animator thread to stop
				animator.join();
			}
			catch (InterruptedException e) {
				System.err.println("Animator thread interrupted at join()");
			}
		}
	}
	
	/**
	 * Start the animation thread
	 */
	public void startGame() {
		// Stats gathering
		threadStartTime = 0L;
		frames = 0;
		updates = 0;
		skippedFrames = 0;
		FPS = 0;
		UPS = 0;
		
		if (animator == null || !running) { 
			animator = new Thread(this, "Animator"); 
			animator.start();
		}
		
		model.startGameTimer();
		
	}
	
	/**
	 * Called when an arrow key is pressed
	 */
	public void directionPressed(int code) {
		int moveAmount = 2;
		
		switch(code) {
		case KeyEvent.VK_UP:
			direction = 0;
			moveY = -moveAmount;
			break;
		case KeyEvent.VK_RIGHT:
			direction = 1;
			moveX = moveAmount;
			break;			
		case KeyEvent.VK_DOWN:
			direction = 2;
			moveY = moveAmount;
			break;
		case KeyEvent.VK_LEFT:
			direction = 3;
			moveX = -moveAmount;
			break;			
		case KeyEvent.VK_SPACE:
			if( model.isGameOver() || model.isGameWon() ) {
				endGame();
				model.endGame();
			}
			Bullet b = model.fireBullet();
			view.addBullet(b);
			break;
		case KeyEvent.VK_P:
			model.setGamePaused( !model.isGamePaused() );
		}
		view.getMacheteroView().startedMoving();
	}
	
	/**
	 *  Called when an arrow key is released
	 */
	public void keyReleased(int code) {
		switch(code) {
		case KeyEvent.VK_UP:
			moveY = 0;
			break;
		case KeyEvent.VK_RIGHT:
			moveX = 0;
			break;			
		case KeyEvent.VK_DOWN:
			moveY = 0;
			break;
		case KeyEvent.VK_LEFT:
			moveX = 0;
			break;			
		}
		if(moveY == 0  && moveX == 0)
			view.getMacheteroView().stoppedMoving();
	}
	
	/**
	 * Returns the GameModel associated with this GameController
	 */
	public GameModel getModel() {
		return model;
	}
	
	/**
	 * Returns the GameView associated with this GameController
	 * @return
	 */
	public GameView getView() {
		return view;
	}
	
	/**
	 * Called on animator thread by update() if the game is lost
	 */
	public void gameOver() {
		model.stopGameTimer();
	}
	
	/**
	 *  Called on animator thread by update() if the game is won
	 */
	public void gameWon() {
		model.stopGameTimer();
	}

	public static int getNumRows() {
		return numRows;
	}

	public static int getNumCols() {
		return numCols;
	}
}
