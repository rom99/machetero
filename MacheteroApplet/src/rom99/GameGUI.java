package rom99;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.OutputStream;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

/**
 * The main gui class for the game - displays the map and captures user input.
 * @author rossmills
 *
 */
public class GameGUI extends JApplet implements ActionListener, KeyListener, GameListener {
	
	private static final Dimension CONTROLS_SIZE = new Dimension(650, 350 );
	private static final String TAG = "GameGUI: ";
	private OutputStream out = System.out;
	private boolean debugAreaOn = false;
	
	private GameController gameController;
	
	private ImageIcon titleImage;
	private ImageIcon iconImage;
	private int titleWidth;
	
	// Swing components
	private GameView view;
	private JScrollPane scroller;
	private JButton startButt;
	private JLabel titleLab, creditLab, iconLab;
	private JFileChooser fileChooser;
	
	private String[] mapSizeNames = { "Tiny (3x3)", "Smaller (4x4)", "Standard (6x6)", "Bigger (8x8)" };
	private int[] mapSizes = { 3, 4, 6, 8 };
	
	private JComboBox sizeCombo;
	private JLabel sizeLabel;
	
	private JPanel mainPanel;
	private JLabel controls;
	private JTextArea debugText = new JTextArea(3, 15);
	private JScrollPane debugArea = new JScrollPane(debugText);
		
	// Constants	
	private static final Color FRAME_BG = Color.BLACK;
	private static final Dimension APPLET_SIZE = new Dimension(900, 850);
		
//	/**
//	 * @param args
//	 */
	public static void main(String[] args) {
		GameGUI game = new GameGUI();
	}
	
	@Override
	public void init() {
		try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                	gameController = new GameController();                	
                	System.out.println("Created gameController");
            		createGUI();            		
                }
            });
        } catch (Exception e) { 
            System.err.println("createGUI didn't complete successfully\n");
        }
	}
	
	/**
	 *  Sets up the gui
	 */
	private void createGUI() {
			
		addKeyListener(this);
		this.setFocusable(true);
		
		this.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent fe) {
				debugText.append("GameGUI: Focus gained\n");
			}
			public void focusLost(FocusEvent fe) {
				debugText.append("GameGUI: Focus lost\n");
			}
		});
		debugText.setFocusable(false);
		if(!debugAreaOn) {
			debugArea.setVisible(false);
		}
		
        // To allow distinctive look and feel
        try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception e) {
                e.getMessage();
            }
   
        // Set up main window characteristics
        
        Container window = getContentPane();
        
        window.setBackground(FRAME_BG);        
        window.setLayout(new FlowLayout() );
		
        mainPanel = new JPanel();
        mainPanel.setBackground(FRAME_BG);
       
		window.add(mainPanel);
		
		// Set up main GUI components
         
        controls = new JLabel();
        setupControls();
        
        ImageLoader loader = new ImageLoader();
        BufferedImage macheteroImage = loader.loadImage("Machetero_Title_Icon.png");        
        
        ImageIcon macheteroIcon = new ImageIcon(macheteroImage);
        
        iconLab = new JLabel(macheteroIcon);
        
       // titleImage = new ImageIcon( GameController.class.getResource("res/text_rasters/Machetero_title.png") );
        	        
        creditLab = new JLabel("� Ross Mills, 2012", SwingConstants.RIGHT);
        creditLab.setForeground(Color.GREEN.brighter());	
                        
        startButt = new JButton("S t a r t  G a m e");
        startButt.setFont(new Font("Impact", Font.PLAIN, 16) );
        startButt.setBackground(Color.green.brighter());
        startButt.setFocusable(false);
        startButt.addActionListener(this);

        sizeCombo = new JComboBox(mapSizeNames);
        sizeCombo.setSelectedIndex(2);
        sizeCombo.setBackground(Color.BLACK);
        sizeCombo.setForeground(new Color(0.9f, 0.9f, 1.0f) );
        sizeCombo.setFocusable(false);
        
        sizeLabel = new JLabel("Choose map size:");
        sizeLabel.setForeground(new Color(0.9f, 0.9f, 1.0f) );

        scroller = new JScrollPane(view);
        scroller.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GREEN.darker(), Color.GREEN.darker() ) );
        scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scroller.setVisible(false);
        
		GroupLayout layout = new GroupLayout( mainPanel );
		
		mainPanel.setLayout(layout);
		
		layout.setHonorsVisibility(true);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		layout.setHorizontalGroup(
				layout.createParallelGroup(Alignment.CENTER)
					.addGroup(layout.createSequentialGroup()
							.addComponent(iconLab)
							.addComponent(debugArea) )
					.addComponent(controls) 
					.addGroup(
						layout.createSequentialGroup()
						.addComponent(sizeLabel)
						.addComponent(sizeCombo, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, 80) 
						)
					.addComponent(startButt )
					.addComponent(scroller, GroupLayout.Alignment.CENTER, 3 * GameModel.TILE_WIDTH, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)					
					.addComponent(creditLab)
				);
		
		layout.setVerticalGroup(
				layout.createSequentialGroup()
					.addGroup( layout.createParallelGroup()
							.addComponent(iconLab)
							.addComponent(debugArea) )
					.addGap(10)
					.addComponent(scroller, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addComponent(controls, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, 250 )					
					.addGap(25)
					.addGroup(
						layout.createParallelGroup()
						.addComponent(sizeLabel)
						.addComponent(sizeCombo, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, 12) )
					.addGap(15)
					.addComponent(startButt, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE)
					.addGap(10)
					.addComponent(creditLab)						
				);

		validate();
	}
	
	@Override
	public void destroy() {
		super.destroy();
		gameController.endGame();
	}

	@Override
	public void start() {
		super.start();
	}

	@Override
	public void stop() {
		super.stop();
		if (gameController.getModel() != null) {
			if (gameController.getModel().isGameStarted() && !gameController.getModel().isGamePaused() ) {
				gameController.getModel().setGamePaused(true);
			}
		}
	}

	/** 
	 * React to keyPressed events
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		//System.out.println("GameGUI: keyPressed");
		int code = e.getKeyCode();
		// Act on relevant events only
		if( code == KeyEvent.VK_UP ||
			code == KeyEvent.VK_RIGHT ||
			code == KeyEvent.VK_DOWN ||
			code == KeyEvent.VK_LEFT ||
			code == KeyEvent.VK_SPACE ||
			code == KeyEvent.VK_P )
		{
			gameController.directionPressed(code);
		}
	}

	/**
	 * React to keyReleased events
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		//System.out.println("GameGUI: keyReleased");
		int code = e.getKeyCode();
		// Act on relevant events only
		if( code == KeyEvent.VK_UP ||
			code == KeyEvent.VK_RIGHT ||
			code == KeyEvent.VK_DOWN ||
			code == KeyEvent.VK_LEFT ||
			code == KeyEvent.VK_SPACE ) 
		{
			gameController.keyReleased(code);
		}
		
		if( code == KeyEvent.VK_C ) 
		{
			gameController.endGame();
			gameEnded( gameController.getModel() );
		}
	}

	/**
	 * keyTyped events are ignored
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		// Ignore		
	}

	/**
	 * React to button press events
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		// Start a new game if start butt is pressed
		if(src == startButt) {
			if(gameController != null) {
				// Stop animator thread while setting up new game
				gameController.endGame();
				
				// Set map size according to combo box selection
				gameController.setMapSize( mapSizes[sizeCombo.getSelectedIndex()] );
				
				// Create a new game
				gameController.newGame(this);
				
				startButt.setVisible(false);
				sizeLabel.setVisible(false);
				sizeCombo.setVisible(false);
				controls.setVisible(false);
				iconLab.setVisible(false);
				
				// Assign view to scroller in window
				view = gameController.getView();
				view.setPreferredSize(new Dimension( gameController.getNumCols() * GameModel.TILE_WIDTH, gameController.getNumRows() * GameModel.TILE_HEIGHT ));
				scroller.setViewportView(view);
				scroller.setSize( view.getPreferredSize() );
				scroller.setVisible(true);
				
				//requestFocus();
				validate();
				
				// Start animator thread
				gameController.startGame();
				
			}
		}
	}
	
	
	/*
	 * Set up the controls panel that gives instructions on how to play the game
	 */
	private void setupControls() {
		
		String instruction = "<html><p>Guide Machetero through the jungle to walk " +
				"off the top of the map. <br />Watch out for the snakes!</p><br />" +
				"<h3>Controls</h3>" +
				"<table><tr><td width='30'></td><td width='120'>arrow keys</td> <td>up, down, left, right</td></tr>" +
				"<tr><td></td><td>space bar </td><td>fire pistol</td></tr>" +
				"<tr><td></td><td>p</td><td>pause</td></tr>" +
				"<tr><td></td><td>ctrl+c</td><td>cancel current game</td></tr>" +
				"</table></html>";
		controls.setText(instruction);
		controls.setFocusable(false);
		controls.setBackground(FRAME_BG);
		controls.setForeground(Color.WHITE);
		controls.setFont( new Font("sans-serif", Font.PLAIN, 16 ) );
		controls.setBorder(BorderFactory.createCompoundBorder( 
				BorderFactory.createLineBorder(Color.green.brighter(), 3),
				BorderFactory.createEmptyBorder( 10, 10, 10, 10) ) );
		controls.setPreferredSize(CONTROLS_SIZE);
	}
	
	/**
	 * What happens after the game has ended
	 */
	@Override
	public void gameEnded(GameModel model) {
		scroller.setVisible(false);
		sizeLabel.setVisible(true);
		startButt.setVisible(true);
		sizeCombo.setVisible(true);
		controls.setVisible(true);
		iconLab.setVisible(true);
		validate();
		System.out.println("View removed, validating...");
	}		
}
