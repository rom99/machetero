/**
 * 
 */
package rom99;

/**
 * @author rossmills
 *	Implement this interface and register with DataModel to receive gameEnded() updates
 *
 */
public interface GameListener {
	public void gameEnded(GameModel model);
}
