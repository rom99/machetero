package rom99;

/**
 * A map is composed of a number of areas
 * 
 * @author rossmills
 *
 */
public class GameMap {
	private Area[][] areaList;
	
	/**
	 * Constructor - you must provide a 2d array of Area objects
	 * @param areas
	 */
	public GameMap(Area[][] areas) {
		areaList = areas;
	}

	/**
	 * @return the areaList
	 */
	public Area[][] getAreaList() {
		return areaList;
	}

	/**
	 * @param areaList the areaList to set
	 */
	public void setAreaList(Area[][] areaList) {
		this.areaList = areaList;
	}
	
	/**
	 * Returns the starting column
	 */
	public int getStartCol() {
		for(int i = 0; i < areaList.length; i++) {
			for(int j = 0; j < areaList[i].length; j++) {
				if(areaList[i][j].isStart() )
					return j;
			}
		}
		return -1;			
	}
	
	/**
	 * Returns the starting row
	 */
	public int getStartRow() {
		for(int i = 0; i < areaList.length; i++) {
			for(int j = 0; j < areaList[i].length; j++) {
				if(areaList[i][j].isStart() )
					return i;
			}
		}
		return -1;	
	}
	
}
