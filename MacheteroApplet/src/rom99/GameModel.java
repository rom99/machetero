package rom99;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.Timer;


/**
 * The main data model for the game.
 * @author rossmills
 *
 */
public class GameModel {
	
	public static final int PATH_WIDTH = 25;
	public static final int TILE_WIDTH = 80;
	public static final int TILE_HEIGHT = 80;
	private ArrayList<GameListener> gameListeners = new ArrayList<GameListener>();

	private volatile boolean gamePaused = false;
	private volatile boolean gameStarted = false;
	
	private int snakesKilled;
	private boolean gameWon = false;
	private boolean gameOver = false;
	private Machetero machetero;
	private GameMap map;
	private ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	private int gameTime = 0;
	
	private Timer timer;
	
	/**
	 * You must provide a Machetero and a GameMap
	 * @param m
	 * @param map
	 */
	public GameModel(Machetero machetero, GameMap map, ArrayList<Sprite> sprites) {
		this.machetero = machetero;
		this.map = map;
		this.setSprites(sprites);
		int areaListLength = map.getAreaList().length;
		int innerAreaListLength =  map.getAreaList()[0].length;
		for(int i = 0; i < areaListLength ; i++) {
			for (int j = 0; j < innerAreaListLength; j++) {
				Area area = map.getAreaList()[i][j];
			}
		}
	}
	
	/**
	 *  Start game timer
	 */
	public void startGameTimer() {
		timer = new Timer(100, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setGameTime(getGameTime() + 1);				
			}
		});
		timer.start();
	}
	
	/**
	 * Stop game timer
	 */
	public void stopGameTimer() {
		timer.stop();
	}
	
	/**
	 * Creates a new Bullet sprite and adds to the list
	 */	
	public Bullet fireBullet() {
		int direction = machetero.getDirection();
		int locX = machetero.getLocX();
		int locY = machetero.getLocY();
		Bullet b = new Bullet(locX, locY);
		b.setDirection(direction);
		sprites.add(b);
		return b;
	}

	/**
	 * Move sprites
	 */
	public void updateSprites() {
		Random random = new Random();
		
		// Remove all sprites if game is finished
		if(gameWon || !machetero.isAlive() ) {
			for(Sprite sprite : sprites) sprite.setDead();
			sprites.clear();
		}
		
		// Move each sprite
		for(int i = 0; i < sprites.size(); i++) {
			int moveX = 0;
			int moveY = 0;
			
			// Move snakes randomly
			Sprite sprite = sprites.get(i);
			if(sprite instanceof Snake) {
				// Move a random amount between -1 and +1
				moveX = random.nextInt(3) - 1; 
				moveY = random.nextInt(3) - 1;
				updateSprite(sprite, moveX, moveY);
			}
			
			// Move bullets
			if(sprite instanceof Bullet) {
				int moveAmount = 5;
				// Move in a straight line
				switch(sprites.get(i).getDirection() ) {
					case 0: moveY = -moveAmount; break;
					case 1: moveX = moveAmount; break;
					case 2: moveY = moveAmount; break;
					case 3: moveX = -moveAmount; break;
				}
				// Note that updateSprite() will perform the update if true
				if( !updateSprite(sprite, moveX, moveY) ) {
					// If bullet hits something, remove the bullet
					// System.out.println("GameModel: remove bullet");
					sprite.setDead();
				}
				for(int j = 0; j < sprites.size(); j++) {
					// Check if bullet hits a snake
					if( sprites.get(j) instanceof Snake ) {
						if( sprite.checkHit(sprites.get(j) ) ) {
							sprites.get(j).setDead();
							sprite.setDead();
							snakesKilled++;
						}
					}					
				}
			}
			if(!sprite.isAlive() ) {
				sprites.remove(sprite);
			}
		}
	}	
	
	
	/** 
	 * Updates a sprite's position if there is a valid path.
	 * It works something like this:
	 * 	<ol><li>	Get what the new position will be </li>
	 * 	<li>		Then check what Area is in that position </li>
	 *  <li>		If it is off the map, don't move. </li> 
	 *  <li>		If it is the top of the map, you win </li>
	 *  <li>		Then check if we can be in the new position on the Area </li>
	 *  <li>		If so, move. If not, don't. </li>
	 *  </ol>
	 */	
	private boolean updateSprite(Sprite sprite, int moveX, int moveY) {
		int newX = sprite.getLocX() + moveX;
		int newY = sprite.getLocY() + moveY;
//		System.out.println("updateSprite:");
		
		Point newPoint = new Point(newX, newY);
//		System.out.printf("newPoint = (%d,%d)\n", newPoint.x, newPoint.y);

		Area[][] areas = map.getAreaList();
				
		if( newPoint.x >= areas[0].length * TILE_WIDTH || 
				newPoint.x <= 0 ) {
//			System.out.println("newPoint is outside map (EAST or WEST)");
			return false;
		}
		
		if( newPoint.y >= areas.length * TILE_HEIGHT ) {
//			System.out.println("newPoint is SOUTH of map");
			return false;
		}
		
		if( newPoint.y <= 0 ) {
//			System.out.println("newPoint is NORTH of map (YOU WIN!)");
			if(sprite instanceof Machetero) gameWon = true;
			return false;
		}
		
		Area theArea = areas[newPoint.y / TILE_HEIGHT][newPoint.x / TILE_WIDTH];
//		System.out.printf("Sprite in area = [%d][%d]\n", newPoint.y / TILE_HEIGHT, newPoint.x / TILE_WIDTH);
		
		if( sprite instanceof Bullet && !theArea.isVisible() ) {
			// Bullets cannot travel into unexplored areas
			return false;
		}
		
		int relativeX = (newPoint.x % TILE_WIDTH);
		int relativeY = (newPoint.y % TILE_HEIGHT);
//		System.out.printf("RelativeX = %d\trelativeY = %d", relativeX, relativeY);
		if( checkPathBoundary(theArea, relativeX, relativeY) ) {
			sprite.moveX(moveX);
			sprite.moveY(moveY);
			if(sprite instanceof Machetero) 
				theArea.setVisible(true);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Update the Machetero sprite
	 * @param moveX
	 * @param moveY
	 * @param direction
	 */
	public void updateMachetero(int moveX, int moveY, int direction) {
		machetero.setDirection(direction);

		boolean snakeBites = false;
		
		// Check if Machetero hits a snake
		for(int i = 0; i < sprites.size(); i++) {
			if(sprites.get(i) instanceof Snake
						&& machetero.checkHit(sprites.get(i) ) ) {
				machetero.setHealth( machetero.getHealth() - 1);
				snakeBites = true;
			}
		}
		
		if(snakeBites) {
			machetero.setBeingBitten(true);
		} else 
			machetero.setBeingBitten(false);
		
		if(machetero.getHealth() < 1) {
			machetero.setHealth(0);
			machetero.setDead();
			setGameOver(true);
		}
		
		updateSprite(this.machetero, moveX, moveY);
	}
	
	/*
	 * Check if the next move puts Machetero off the relevant Area's path - true if it does not
	 * 
	 */
	private boolean checkPathBoundary(Area area, int newX, int newY) {
        
        int lx, rx, ty, by;
        
        lx = TILE_WIDTH/2 - PATH_WIDTH/2 ;
        rx = lx + PATH_WIDTH;
        ty = TILE_HEIGHT/2 - PATH_WIDTH/2 ;
        by = ty + PATH_WIDTH;        
        
        if(area.connects[Area.NORTH] 
                && newX > lx
                && newX < rx
                && newY < by) {
            return true;            
        } else if(area.connects[Area.EAST]
                && newY > ty
                && newY < by
                && newX > lx) {
            return true;            
        } else if(area.connects[Area.SOUTH]
                && newX > lx
                && newX < rx
                && newY > ty) {
            return true;
        } else if(area.connects[Area.WEST]
                && newY > ty
                && newY < by
                && newX < rx) {
            return true;
        } else {
//            System.out.println("Path boundary fault: " + newX + "," + newY);
            return false;
        }                
        
    }
	
	/**
	 * Register for gameEnded() events
	 * @param listener
	 */
	public void addGameListener(GameListener listener) {
		gameListeners.add(listener);
	}
	
	/**
	 * Called when game is ended and its time to set up a new game
	 */
	public void endGame() {
		for(GameListener listener : gameListeners ) {
			listener.gameEnded(this);
		}
	}

	public ArrayList<Sprite> getSprites() {
		return sprites;
	}

	public void setSprites(ArrayList<Sprite> sprites) {
		this.sprites = sprites;
	}	

	/**
	 * @return the machetero
	 */
	public Machetero getMachetero() {
		return machetero;
	}

	/**
	 * @param machetero the machetero to set
	 */
	public void setMachetero(Machetero machetero) {
		this.machetero = machetero;
	}

	/**
	 * @return the map
	 */
	public GameMap getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(GameMap map) {
		this.map = map;
	}
	
	/**
	 * Returns the gameWon status
	 */
	public boolean isGameWon() {
		return gameWon;
	}

	public int getSnakesKilled() {
		return snakesKilled;
	}

	public void setSnakesKilled(int snakesKilled) {
		this.snakesKilled = snakesKilled;
	}

	public int getGameTime() {
		return gameTime;
	}

	public void setGameTime(int gameTime) {
		this.gameTime = gameTime;
	}

	public boolean isGamePaused() {
		return gamePaused;
	}

	public void setGamePaused(boolean gamePaused) {
		this.gamePaused = gamePaused;
		if(isGamePaused() ) { 
			this.stopGameTimer(); 
		} else { 
			this.startGameTimer();
		}
	}

	public boolean isGameStarted() {
		return gameStarted;
	}

	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	
}
