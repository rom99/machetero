package rom99;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Formatter;

import javax.swing.JPanel;

/** 
 * Displays MapView and MacheteroView in a JPanel component
 * @author rossmills
 *
 */
public class GameView extends JPanel {
	public final int P_HEIGHT = ( GameModel.TILE_HEIGHT * GameController.getNumRows() );
	public final int P_WIDTH = ( GameModel.TILE_WIDTH * GameController.getNumCols() );
	
	// global variables for off-screen rendering
    private Graphics dbg;
    private BufferedImage dbImage = null;
    
	private MacheteroView macheteroView;
	private MapView mapView;	
	private GameModel model;
	private ArrayList<SpriteView> spriteViews = new ArrayList<SpriteView>();
	
	/**
	 * Constructor requires a GameModel reference because it creates the MapView, creates SpriteViews, and
	 * creates the MacheteroView, linking them to their model counterparts.
	 * @param model
	 */
	public GameView(GameModel model, ImageLoader tileLoader, ImageLoader spriteLoader, ImageLoader macheteroLoader ) {
		super();
		this.model = model;
		mapView = new MapView(model.getMap(), tileLoader );
		
		ArrayList<Sprite> sprites = model.getSprites();
		int numSprites = sprites.size();		
		for(int i = 0; i < numSprites; i++)  {
			spriteViews.add( new SnakeView( sprites.get(i), spriteLoader ) );
		}
		
		this.setPreferredSize(new Dimension(
				P_WIDTH, 
				P_HEIGHT 
				));
		
		macheteroView = new MacheteroView( model.getMachetero(), macheteroLoader );
		
		System.out.printf("Created new GameView: size=%d, %d\n", 
				this.getPreferredSize().width, 
				this.getPreferredSize().height);
	}
	
	/**
	 *  Add new bullet sprite to the view
	 */
	public void addBullet(Bullet bullet) {
		BulletView bv = new BulletView(bullet);
		spriteViews.add( bv );
	}
	
	/**
	 * Render the screen to a buffer - called by the animation Thread
	 */
	public void gameRender() {
						
		//System.out.println("GameView: gameRender");
		if (dbImage == null){  // create the buffer
			dbImage = new BufferedImage(P_WIDTH, P_HEIGHT, BufferedImage.TYPE_INT_ARGB );
		}				
		
		//System.out.println("Preparing render");
		dbg = dbImage.getGraphics();
				
		if(model.isGamePaused() ) {
			drawGamePaused(dbg);
			return;
		}
		
		Formatter sf = new Formatter();
		String time = "" + sf.format("%02d:%02d:%01d", model.getGameTime() /10 /60, 
										model.getGameTime() / 10 % 60,
										model.getGameTime() % 10);
		
		// clear the background (is this necessary actually?)
		clearBG(dbg);
		
		// draw the mapView
		mapView.draw(dbg);
		
		// draw the sprites
		drawSprites(dbg);
		
		macheteroView.draw(dbg);
		
		// Display "You win!" message and score
		if(model.isGameWon() && model.getMachetero().isAlive() ) {
			drawYouWin(dbg);
		}
		
		// Display "Game Over" message
		if(! model.getMachetero().isAlive() ) {
			drawGameOver(time, dbg);
		}
		
		// Display status
		if( GameController.DISPLAY_STATUS && !model.isGameWon() && model.getMachetero().isAlive() ) {
			drawGameStatus(time, dbg);
		}
		
		// Display stats info
		if(GameController.STATS_ON ) {
			drawStats(dbg);
		}
	} // end gameRender()
	
	/*
	 * Clear background
	 */
	private void clearBG(Graphics dbg) {
		dbg.setColor(Color.black);
		dbg.fillRect (0, 0, this.getWidth(), this.getHeight() );
	}
	
	/*
	 * Draw game paused message
	 */
	private void drawGamePaused(Graphics dbg) {
		Font f = new Font("Sans-serif", Font.BOLD, 35);
		FontMetrics fm = this.getFontMetrics(f);
		dbg.setColor(new Color(0.9f, 0.9f, 1.0f));
		dbg.setFont(f);
		String msg = "PAUSED";
		int leftAlign = P_WIDTH / 2 - fm.stringWidth(msg) / 2;
		dbg.drawString(msg, leftAlign, P_HEIGHT / 2 );
	}
	
	/*
	 * Draw sprites
	 */
	private void drawSprites(Graphics dbg) {
		for(int i = 0; i < spriteViews.size(); i++) {
			Area[][] areas = model.getMap().getAreaList();
			if(spriteViews.get(i).getSprite().isAlive() ) {
				Sprite sprite = spriteViews.get(i).getSprite();
				Area spriteArea = areas[ sprite.getRow() ][sprite.getCol() ];
				if(spriteArea.isVisible() ) {
					spriteViews.get(i).draw(dbg);
				}
			}
			else {
				// Remove sprite views whose Sprite is dead
				// System.out.println("GameView: Sprite is dead - removing");
				spriteViews.remove( i );
			}
		}
	}	
	
	/*
	 * Draw you win message
	 */
	private void drawYouWin(Graphics dbg) {
		// Semi-transparent background rectangle
		dbg.setColor(new Color(0x00, 0x00, 0x50, 0x45) );
		int rectWidth = 230;
		int rectHeight = 230;
		dbg.fillRoundRect(P_WIDTH / 2 - rectWidth / 2, 
				P_HEIGHT / 2 - rectHeight / 2, rectWidth, rectHeight, 5, 5);

		// Main message
		Font f = new Font("Sans-serif", Font.BOLD, 35);
		FontMetrics fm = this.getFontMetrics(f);
		dbg.setColor(new Color(0.9f, 0.9f, 1.0f));
		dbg.setFont(f);
		String msg = "You win!";
		int leftAlign = P_WIDTH / 2 - fm.stringWidth(msg) / 2;
		int vertical = P_HEIGHT / 2 - 50;
		dbg.drawString(msg, leftAlign, vertical);

		// Get scores
		int timeBonus = (GameController.getNumRows() * 10)
				- (model.getGameTime() / 10);
		if (timeBonus < 0)
			timeBonus = 0;
		String scoreSnakes = "Snakes killed: " + model.getSnakesKilled();
		String scoreHealth = "Health remaining: "
				+ model.getMachetero().getHealth();
		String scoreTime = "Time bonus: " + timeBonus;
		int total = (model.getSnakesKilled() * 10)
				+ (model.getMachetero().getHealth() ) + timeBonus;
		String totalScore = "TOTAL SCORE: " + total;

		// Draw scores
		Font f2 = new Font("Sans-serif", Font.BOLD, 15);
		FontMetrics fm2 = this.getFontMetrics(f2);
		dbg.setFont(f2);
		dbg.drawString(scoreSnakes, leftAlign, vertical + 2 * fm2.getHeight() );
		dbg.drawString(scoreHealth, leftAlign, vertical + 3 * fm2.getHeight() );
		dbg.drawString(scoreTime, leftAlign, vertical + 4 * fm2.getHeight() );
		dbg.drawString(totalScore, leftAlign, vertical + 6 * fm2.getHeight() );
		dbg.drawString("Press space", P_WIDTH / 2 - fm2.stringWidth("Press space") / 2, P_HEIGHT - 10 );
	} // end drawYouWin
	
	/*
	 * Draw game over message
	 */
	private void drawGameOver(String time, Graphics dbg) {
		// Transparent background rectangle
		dbg.setColor(new Color(0x00, 0x00, 0x00, 0x55));
		int rectWidth = 260;
		int rectHeight = 190;
		dbg.fillRoundRect(P_WIDTH / 2 - rectWidth / 2, P_HEIGHT / 2
				- rectHeight / 2, rectWidth, rectHeight, 5, 5);

		// Display title
		Font f = new Font("Sans-serif", Font.BOLD, 35);
		FontMetrics fm = this.getFontMetrics(f);
		dbg.setColor(Color.RED);
		dbg.setFont(f);
		String msg = "GAME OVER";
		dbg.drawString(msg, P_WIDTH / 2 - fm.stringWidth(msg) / 2, P_HEIGHT / 2);

		// Display message
		Font f2 = new Font("Sans-serif", Font.PLAIN, 20);
		FontMetrics fm2 = this.getFontMetrics(f2);
		dbg.setFont(f2);
		String msg2 = "The snakes got you!";
		dbg.drawString(msg2, P_WIDTH / 2 - fm2.stringWidth(msg2) / 2, 
				P_HEIGHT / 2 + fm.getMaxAscent() + fm.getMaxDescent() );
		String msg3 = "Time in play: " + time;
		dbg.drawString(msg3, P_WIDTH / 2 - fm2.stringWidth(msg2) / 2, 
				P_HEIGHT / 2 + fm.getMaxAscent() + fm.getMaxDescent() + fm2.getHeight() );
		
		dbg.setColor(Color.white);
		dbg.drawString("Press space", P_WIDTH / 2 - fm2.stringWidth("Press space") / 2, P_HEIGHT - 10 );
	}
	
	/*
	 * Draw game status info
	 */	
	private void drawGameStatus(String time, Graphics dbg) {
		Font f = new Font("Sans-serif", Font.BOLD, 17);
		FontMetrics fm = this.getFontMetrics(f);
		dbg.setFont(f);
		if( model.getMachetero().isBeingBitten() ) {
			dbg.setColor(Color.red);
		} else {
			dbg.setColor(Color.white);
		}
		
		String healthDisplay = "Health: " + model.getMachetero().getHealth();
		dbg.drawString(healthDisplay, P_WIDTH - fm.stringWidth(healthDisplay), fm.getMaxAscent() );
		
		dbg.setColor(Color.white);
		String timerDisplay = "Time: " + time;
		dbg.drawString(timerDisplay, P_WIDTH - fm.stringWidth(healthDisplay) - fm.stringWidth(timerDisplay) - 25, fm.getMaxAscent() );
	}
	
	/*
	 * Draw stats info
	 */
	private void drawStats(Graphics dbg) {
		Font f = new Font("Sans-serif", Font.PLAIN, 12);
		FontMetrics fm = this.getFontMetrics(f);
		dbg.setColor(Color.WHITE);
		dbg.setFont(f);
		String msg = "FPS: " + GameController.FPS 
				+ "\nUPS: " + GameController.UPS 
				+ "\nFrames skipped: " + GameController.skippedFrames ;
		dbg.drawString(msg, this.getWidth() - fm.stringWidth(msg), this.getHeight() - 5);
	}
	
	/**
	 *  Actively draw the buffered image to the panel
	 */
	public void paintScreen() {
		Graphics g;
		try {
			g = this.getGraphics();
			if( (g != null) && (dbImage != null) ) {
				g.drawImage(dbImage, 0, 0, null);
				// Force flush display buffer on Linux systems
				Toolkit.getDefaultToolkit().sync();
				g.dispose();
			}
		}
		catch (Exception e) {
			System.err.println("GameView.paintScreen(): Graphics context error\n" + e.getLocalizedMessage() );
		}
		//System.out.println("GameView: paintComponent");
	}
	
	/**
	 * Set the data model to which this view refers
	 */
	public void setModel(GameModel model) {
		this.model = model;
	}

	public MacheteroView getMacheteroView() {
		return macheteroView;
	}
	
}
