package rom99;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

/**
 * Loads a range of files specified in an image definition file within the IMG_DIR folder and populates a HashMap associating names
 * with ArrayLists of BufferedImages.  A definition file should look like e.g.:
 * 
 * i path/filename.png
 * i filename2.jpg
 * 
 * Image definition files can have comments using // syntax.  The i flag specifies a single image file. Only single image file associations are supported 
 * so far but support for multiple files associated with a single name will be added to support animation, etc. 
 * 
 * This version allows specifying files in a directory named "res" in the source folder, or any sub-directory relative to "res".
 * 
 * Adapted from Andrew Davison's ImageLoader class from Killer Game Programming in Java , O'Reilly Media, 2005.
 * 
 * @author rossmills
 * @param String the filename for the image definition file (relative to "res" folder)
 *
 */
public class ImageLoader {
	
	private static final String IMG_DIR = "res/";
	
	private static HashMap<String, ArrayList<BufferedImage> > imagesMap = new HashMap<String, ArrayList<BufferedImage>>();
	private GraphicsConfiguration gc;
	
	/**
	 * Takes the filename for an image definition file as the parameter
	 * and automatically loads all images defined in that file.
	 * 
	 * @param filename
	 */	
	public ImageLoader(String filename) {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment( );
		gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
		loadImagesFile(filename);
	}
	
	/**
	 * A no-argument constructor for when there is no image definition file and you just want to load an image in code
	 */
	public ImageLoader() {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment( );
		gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
	}
	
	/**
	 * Returns the ArrayList of images associated with the key name
	 * @param name
	 * @return the ArrayList of images, or null if no images present
	 */
	public ArrayList<BufferedImage> getImages(String name ) {
		if(imagesMap.containsKey( name ) )
			return imagesMap.get(name);
		else
			return null;
	}
	
	/**
	 * Returns the first image in the array list of images associated with the key name
	 * @param filename
	 */
	public BufferedImage getImage(String name) {
		if(imagesMap.containsKey( name ) )
			return imagesMap.get(name).get(0);
		else {
			System.out.println("ImageLoader: no image found for string \""+name+"\"");
			return null;
		}
	}
	
	/**
	 * Returns the image at the given index associated with the key name
	 * @param filename
	 */
	public BufferedImage getImage(String name, int index) {
		if(imagesMap.containsKey( name ) ) {
			return imagesMap.get(name).get(index);
		} else {
			System.out.println("ImageLoader: no image found for string \""+name+"\""+" with index " + index);
			return null;
		}
	}
	
	/*
	 * Reads image definition file and checks image type flags to call appropriate image loading methods
	 */
	private void loadImagesFile(String filename) {
		String filePath = IMG_DIR + filename;
		try {
			InputStream inFile = this.getClass().getResourceAsStream(filePath);
			if(inFile == null) {
				System.out.println("Can't find resource: " + filePath);
				return;
			}
			
			System.out.println("ImageLoader: loading from " + filePath);
			BufferedReader br = new BufferedReader( new InputStreamReader(inFile) );
			String line;
			char ch;
			
			while( (line = br.readLine() ) != null) {
				if (line.length() == 0) // blank line
					continue;
				if (line.startsWith("//") )  // comment
					continue;
			    ch = Character.toLowerCase( line.charAt(0) );
			    if (ch == 'i')  // one image
			    	getFileNameImage(line);
			    else if (ch == 'n') // a numbered sequence of images
			    	getNumberedImages(line);
//			    else if (ch == 's') // an images strip
//			    	getStripImages(line);
//			    else if (ch == 'g') // a group of images
//			    	getGroupImages(line); 
			    else
			    	System.out.println("Do not recognize line: " + line);
			}
			br.close( ); 
		}
       catch (IOException e)
       { 
    	   System.out.println("Error reading file: " + filePath);
    	   System.exit(1);
       }
	} // end loadImagesFile
	
	/*
	 * Get numbered sequence of images
	 * @param line is a line from the image definition file
	 */
	private void getNumberedImages(String line) {
		StringTokenizer tokens = new StringTokenizer(line);
		if (tokens.countTokens() != 3) {
			System.out.println("Wrong no. of arguments for " + line);
		} else {
			tokens.nextToken(); // skip command label
			System.out.println("n line: " + line);
			String filePath = tokens.nextToken();
			int n = 0;
			try {
				n = Integer.parseInt( tokens.nextToken() );
			} catch (NumberFormatException ex ) {
				System.out.println("NumberFormatException for line: "
						+ line + ex.getLocalizedMessage() );
				return;
			}
			loadNumberedImages( filePath, n );			
		}
	}
	
	/*
	 * Load number of images into an array and add to HashMap
	 */
	public boolean loadNumberedImages( String filePath, int num ) {
		ArrayList<BufferedImage> imgList = new ArrayList<BufferedImage>();
		for(int i = 0; i < num; i++ ) {
			String filename = filePath.replace("*", Integer.toString(i) );
			BufferedImage bi = loadImage(filename);
			if (bi != null) {
				imgList.add(bi);
				System.out.println("Added numbered image: "+filename);
			} else {
				System.out.printf("Image %d was null in %s", i, filename);
				return false;
			}
		}		
		String name = getPrefix( getFilename(filePath) );
		name = name.substring(0, name.length() -1);
		imagesMap.put(name,  imgList);
		System.out.println("n image stored with key: " + name);
		return true;
	}
	
	/*
	 *  @param line is a line from the image definition file
	 */
	private void getFileNameImage(String line) {		
		StringTokenizer tokens = new StringTokenizer(line);
		if (tokens.countTokens() != 2) {
			System.out.println("Wrong no. of arguments for " + line);
		} else {
			tokens.nextToken(); // skip command label
			System.out.print("i line: "); 
			loadSingleImage( tokens.nextToken() );
		}
	}
	
	/**
	 * Load an image and add to the imagesMap
	 * @param filename
	 * @return
	 */
	public boolean loadSingleImage(String filePath) {
		String name = getPrefix( getFilename(filePath) );
		if (imagesMap.containsKey(name) ) {
			System.out.println("Error: " + name + "already used");
			return false;
		}
		BufferedImage bi = loadImage(filePath);
		if (bi != null) {
			ArrayList imsList = new ArrayList();
			imsList.add(bi);
			imagesMap.put(name, imsList);
			System.out.println("Stored " + name + " " + filePath);
			return true;
		} else
			return false;
	}
	
	/*
	 * Extract the filename portion of a file path and return
	 */
	private String getFilename(String text) {
		int slash = 0;
		String filename = text;
		while( slash != -1 ) {
			slash = filename.indexOf("/");
			filename = filename.substring(slash + 1, filename.length() );
		}
		return filename;
	}
    
	/*
	 * Extract the prefix portion of a file name and return
	 */
	private String getPrefix(String text) {		
		int dot = text.indexOf(".");
		String prefix = text.substring(0, dot); // parse the name part only
		System.out.println("ImageLoader: got prefix " + prefix); // debug purposes
		return prefix;
	}
	
	/*
	 * Extract the suffix portion of a file name and return
	 */
	private String getSuffix(String text) {
		int dot = text.indexOf(".");
		String suffix = text.substring(dot, text.length() );
		System.out.println("ImageLoader: got suffix " + suffix);
		return suffix;
	}
	
	/**
	 * Loads and returns the BufferedImage
	 * @param filePath - the file path relative to the IMG_DIR directory
	 * @return a "compatible image" for the current GraphicsContext
	 */
	public BufferedImage loadImage(String filePath) {
		try {
			BufferedImage im = ImageIO.read(this.getClass().getResource(
					IMG_DIR + filePath) );
			int transparency = im.getColorModel().getTransparency();
			BufferedImage copy = gc.createCompatibleImage(im.getWidth(),
					im.getHeight(), transparency);
			// create a graphics context
			Graphics2D g2d = copy.createGraphics();
			// reportTransparency(IMAGE_DIR + fnm, transparency);
			
			// copy image
			g2d.drawImage(im,0,0,null);
			g2d.dispose( );
			return copy;
		} catch (IOException e) {
			System.out.println("Load Image error for " + IMG_DIR + "/" + filePath
					+ ":\n" + e);
			return null;
		}
	}
}
