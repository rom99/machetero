package rom99;

import java.awt.image.BufferedImage;

/**
 * For animating image sequences
 * 
 * @author rossmills
 *
 */
public class ImagePlayer {
	
	private String imgName;	
	private boolean isRepeating;
	private boolean ticksIgnored = true;
	private ImageLoader loader;
	
	// Length of full sequence
	private int seqDuration;
		
	// How long since the start of the sequence
	private long animTotalTime;
	
	// How often updateTick() gets called (ms)
	private int animPeriod;
	
	// How long each image should be shown (seqDuration / numImages )
	private int showPeriod;

	// How many images there are in the sequence
	private int numImages;	
	
	// Which of the images is current image (index)
	private int imPosition;
	
	public ImagePlayer( String imgName, int animPeriod, int sequenceDuration, boolean repeats, ImageLoader loader ) {
		this.imgName = imgName;
		this.animPeriod = animPeriod;
		this.seqDuration = sequenceDuration;
		this.isRepeating = repeats;
		this.loader = loader;
		numImages = loader.getImages(imgName).size();
		showPeriod = seqDuration / numImages;
		imPosition = 0;
		animTotalTime = 0;		
	}
	
	public void updateTick() {
		// I assume that this method is called every animPeriod ms
		if (!ticksIgnored) {
			// 	update total animation time, modulo sequence duration 
			animTotalTime = (animTotalTime + animPeriod) % (seqDuration);
			// calculate current displayable image position
		    imPosition = (int) (animTotalTime / showPeriod);
		    if ( (imPosition == numImages-1) && (!isRepeating) ) {  //sequence end
		    	ticksIgnored = true;   // stop at this image
//		    	if (watcher != null)
//		             watcher.sequenceEnded(imName);   // call callback
		    }
		} 
	}
	
	public BufferedImage getCurrentImage() { 
		if (numImages != 0) {
			return loader.getImage(imgName, imPosition);
		} else {
			return null;
		}
    }
	
	/**
	 * Stops this ImagePlayer
	 */
	public void stop() {
		ticksIgnored = true;
	}
	
	/**
	 * Restarts this ImagePlayer
	 */
	public void restart() {
		ticksIgnored = false;
	}
}
