package rom99;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

/**
 * Includes data about Machetero's location, the direction Machetero is facing, clothing he is wearing and equipment he is carrying.
 * 
 * @author rossmills
 *
 */
public class Machetero extends Sprite {

	private int health = 50;
	private boolean beingBitten;
	
	@Override
	public String getName() {
		return "Machetero";
	}
	
	/**
	 * Machetero constructor requires a GameMap to find the starting row/column
	 * @param map
	 */	
	public Machetero( GameMap map ) {
		locX = (map.getStartCol() * GameModel.TILE_WIDTH) + (GameModel.TILE_WIDTH/2) ;
		locY = (map.getStartRow() + 1) * GameModel.TILE_HEIGHT - 10;
		beingBitten = false;
	}
	
	public void setBeingBitten(boolean b) {
		beingBitten = b;
	}
	
	public boolean isBeingBitten() {
		return beingBitten;
	}

	@Override
	public int getWidth() {		
		return 30;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 30;
	}
	
	public void setHealth(int h) {
		health = h;
	}
	
	public int getHealth() {
		return health;
	}	
	// Further attributes such as health, inventory, etc can be added later
}
