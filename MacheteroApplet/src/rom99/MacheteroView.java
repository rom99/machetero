package rom99;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.Timer;

public class MacheteroView {
	
	private int width = 25;
	private int height = width; // Machetero image is a square
	private Machetero machetero;
	private ImageLoader imageLoader;
	private ImagePlayer northPlayer;
	private ImagePlayer eastPlayer;
	private ImagePlayer southPlayer;
	private ImagePlayer westPlayer;
	
	private ArrayList<BufferedImage> images;
	
	/**
	 * MacheteroView
	 * @param machetero a reference to the Machetero object
	 * @param macheteroLoader a reference to the ImageLoader object for Machetero images
	 */
	public MacheteroView(Machetero machetero, ImageLoader macheteroLoader) {
		this.machetero = machetero;
		imageLoader = macheteroLoader;
		
		northPlayer = new ImagePlayer("north_walk", 80, 320, true, imageLoader);
		eastPlayer = new ImagePlayer("east_walk", 80, 320, true, imageLoader);
		southPlayer = new ImagePlayer("south_walk", 80, 320, true, imageLoader);
		westPlayer = new ImagePlayer("west_walk", 80, 320, true, imageLoader);
				
		// set up timer to update ticks (perhaps should be part of animation loop)
		Timer timer = new Timer(80, new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				northPlayer.updateTick();
				eastPlayer.updateTick();
				southPlayer.updateTick();
				westPlayer.updateTick();
			}
		});
		timer.start();		
	}
	
	/**
	 * Stops movement animations
	 */
	public void stoppedMoving() {
		northPlayer.stop();
		eastPlayer.stop();
		southPlayer.stop();
		westPlayer.stop();
	}
	
	/**
	 * Starts movement animations
	 */
	public void startedMoving() {
		northPlayer.restart();
		eastPlayer.restart();
		southPlayer.restart();
		westPlayer.restart();
	}
	
	/**
	 * Draws the appropriate image for MacheteroView depending on Machetero object's current state 
	 * @param g
	 */
	public void draw(Graphics g) {
		
		ImagePlayer player = getPlayer();			
		
		int x = machetero.getLocX() - width/2;
		int y = machetero.getLocY() - height/2;
		
		if( machetero.isBeingBitten() ) {
			g.drawImage(
					imageLoader.getImage("MacheteroRed", machetero.getDirection() ),
					x ,
					y ,
					width,
					height,
					null);
		} else {
			g.drawImage(
					player.getCurrentImage(), 
					x ,
					y , 
					width, 
					height, 
					null);
		}
		
		
	}
	
	private ImagePlayer getPlayer() {
		switch(machetero.getDirection() ) {
			
		case 0:
			return northPlayer;
		case 1:
			return eastPlayer;
		case 2:
			return southPlayer;
		case 3:
			return westPlayer;
			
		default:
			return northPlayer;
		}
	}
}
