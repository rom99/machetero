package rom99;

import java.util.Random;

/**
 * Class used to generate random maps
 * @author rossmills
 *
 */
public class MapFactory {
	
	Random random = new Random();
	private static final boolean echoOn = false;
	
	/*
     *  Array holds the area objects
     */
    private Area areas[][];
    
    private int startRow, startCol;
    private int endRow, endCol;
    
    private int numCols;
    private int numRows;
    
    private int numAreas;   
        
    private int areaWidth;
    private int areaHeight;
	
	/*
     *  Fill any blank tiles with random tiles
     */
    private void fillRandomMap() {                
        for(int i = 0; i < areas.length; i++ ) {            
            for(int j = 0; j < areas[i].length; j++) {
                if(areas[i][j] == null) {
                    areas[i][j] = getRandomPath(i, j);
                }                       
            }                   
        }
    }
    
    /** 
     *  Get a random map with a path that guarantees at least one way to get
     *  from the NORTH to the SOUTH edge of the map.
     * 
     *  (Psuedo code in comments after method header)
     */
    public GameMap getRandomMap(int numRows, int numCols) {
        /* nextTile = random NORTH tile, must connect[0]
         *** FIRST ROW ***
         * while(prevTile.connect[Area.EAST] && nextTileEAST < numCols ) {
         *      select random newPiece until it satisfies connect[4] && ( connect[1] || connect[3] )
         *      prevTile = newPiece
         *      nextTile = toEAST
         * }
         * if (prevTile.connect[Area.EAST]) {
         *      select random piece until it satisfies connect[4] && ( connect[3])
         *      prevTile = newPiece
         *      nextTile = toBelow
         * } else {
         *      nextTile = belowPrevTile
         * }
         * 
         * ** NEXT ROWS ***
         * while(there are more rows) {
         * 
         * select random newPiece to satisfy connect[0]
         * prevTile = newPiece
         * if (prevTile >= numCols/2)
         *      prefer WEST
         * } else {
         *      prefer EAST
         * }
         * while(prevTile.connect[area.direction] && nextTileDirection < numCols && nextTileDirection > 0 {
         *      select random newPiece until it satisfies connect[opposite to direction] && ( connect[direction] || connect[3] )
         *      prevTile = newPiece
         *      nextTile = toDirection
         *  }
         * 
         * if(prevTile.connect[area.direction] && nextTileDirection < numCols)
         *      select random piece until it satisfies connect[2] && connect[3]
         * else 
         * if (prevTile.connect[area.direction] && nextTileDirection > 0)
         *      select random piece until it satisfies connect[4] && connect[3]
         * else
         *      nextTile = below prevTile
         * 
         * end while
         *      
         */
        this.numCols = numCols;
        this.numRows = numRows;
        this.numAreas = numCols*numRows;
        
        areas = new Area[numRows][numCols];
        
        int nextTile = random.nextInt(numCols-1);
        int prevTile = 0;
        int dir = 1;
        int rowCount = 0;
        
        // randomly select the first tile
        do {
            areas[rowCount][nextTile] = getRandomPath(rowCount, nextTile); 
            echo("EAST: " + areas[rowCount][nextTile].connects[Area.EAST]);
            echo("SOUTH: " + areas[rowCount][nextTile].connects[Area.SOUTH]);
        } while (!(areas[rowCount][nextTile].connects[Area.NORTH]
                && ( areas[rowCount][nextTile].connects[Area.EAST]
                        || areas[rowCount][nextTile].connects[Area.SOUTH]) ) );
        
        // set values for ending tile
        endRow = 0;
        endCol = nextTile;
        areas[rowCount][nextTile].setEnd(true);
        
        prevTile = nextTile;
        
        // randomly select more tiles for this row until can only connect down
        while(areas[rowCount][prevTile].connects[Area.EAST]
                &&  prevTile + dir < numCols-1 ) {
            nextTile = prevTile + dir;            
            do {
                areas[rowCount][nextTile] = getRandomPath(rowCount, nextTile);                
            } while (!(areas[rowCount][nextTile].connects[Area.WEST]
                      && ( areas[rowCount][nextTile].connects[Area.EAST]
                        || areas[rowCount][nextTile].connects[Area.SOUTH]) ) );
            prevTile = nextTile;
        } 
        
        if (areas[rowCount][prevTile].connects[Area.EAST]) {
            nextTile = prevTile + dir;
            do {
                areas[rowCount][nextTile] = getRandomPath(rowCount, nextTile);
            } while (! (areas[rowCount][nextTile].connects[Area.WEST] 
                        && areas[rowCount][nextTile].connects[Area.SOUTH]));
            prevTile = nextTile;
            rowCount++;
        } else {
            rowCount++;       
        }
        
        echo("Done first row - rowCount: " + rowCount);
        
        // randomly select more tiles for each subsequent row        
        while(rowCount < numRows) {
            
            if(prevTile >= numCols/2) {
                dir = -1;
            }
            else {
                dir = 1;
            }
        
            echo("Dir: " + dir);
            echo("PrevTile: " + prevTile);
            echo("NextTile: " + ( nextTile ) );
            do {
                areas[rowCount][nextTile] = getRandomPath(rowCount, nextTile);                
            } while (!(areas[rowCount][nextTile].connects[Area.NORTH]
                    && ( areas[rowCount][nextTile].connects[(4 + dir) % 4]
                        || areas[rowCount][nextTile].connects[Area.SOUTH]) ) );
            
            while(areas[rowCount][prevTile].connects[(4 + dir) % 4]
                    && prevTile + dir < numCols-1
                    && prevTile + dir > 0) {
                nextTile = prevTile + dir;
                echo("Innerloop:NextTile: " + nextTile);
                do {
                    areas[rowCount][nextTile] = getRandomPath(rowCount, nextTile);                
                } while (!(areas[rowCount][nextTile].connects[(4 - dir) % 4]
                          && ( areas[rowCount][nextTile].connects[(4 + dir) % 4]
                          || areas[rowCount][nextTile].connects[Area.SOUTH]) ) );
                prevTile = nextTile;
            }
            
            echo("NextTile: " + nextTile);
            if(areas[rowCount][prevTile].connects[(4 + dir) % 4]
                    && nextTile + dir < numCols-1) {
                nextTile = prevTile + dir;
                echo("WEST:NextTile: " + nextTile);
                do {
                    areas[rowCount][nextTile] = getRandomPath(rowCount, nextTile);                
                } while (!(areas[rowCount][nextTile].connects[Area.SOUTH]
                        && areas[rowCount][nextTile].connects[Area.EAST]) );
                prevTile = nextTile;
            } else {
                if (areas[rowCount][prevTile].connects[(4 + dir) % 4]
                        && nextTile + dir > 0) {
                    nextTile = prevTile + dir;
                    echo("EAST:NextTile: " + nextTile);
                    do {
                        areas[rowCount][nextTile] = getRandomPath(rowCount, nextTile);                
                    } while (!(areas[rowCount][nextTile].connects[Area.SOUTH]
                        && areas[rowCount][nextTile].connects[Area.WEST]) );
                     prevTile = nextTile;
                }
            }
                              
            rowCount++;
            echo("Row Count: " + rowCount);
        }
        
        areas[rowCount-1][prevTile].setStart(true);
        // set value for starting tile
        startCol = prevTile;
        startRow = rowCount-1;
        fillRandomMap();
        
        return new GameMap(areas);
    }
	
	/**
     *  Return a random path type (corner, cross, tee, straight)
     *  Slightly weighted to produce corners.
     *  Rotates by a random number of quarter turns.
     * 
     * @param j     the column index of the area
     * @param i     the row index of the area
     * @return      a new Area object
     */
    private Area getRandomPath(int i, int j) {
        
        int nextArea = random.nextInt(6);
        int rotate = random.nextInt(4);
        switch(nextArea) {
                
                   case 0:
                        
                        areas[i][j] = new CornerPath();
                        break;

                    case 1:
                        areas[i][j] = new TeePath();
                        break;

                    case 2:
                        areas[i][j] = new StraightPath();
                        break;

                    case 3:
                        areas[i][j] = new CornerPath();
                        break;

                    case 4:
                        areas[i][j] = new CrossPath();
                        break;               
                        
                    case 5:
                    	areas[i][j] = new TerminusPath();
                    	break;
         }
        
        for(int x = 0; x<rotate; x++) {
            areas[i][j].rotate();
        }
        
        //areas[i][j].setVisible(true);
        
        return areas[i][j];
    }
    
    private void echo(String message) {
        if(echoOn) {
            System.out.println(message);
        }
    }
}
