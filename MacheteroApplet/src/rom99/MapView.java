package rom99;

import java.awt.Graphics;
import java.awt.image.BufferedImage;


/**
 * MapView is made up of a list of AreaViews and some details like width and height
 * @author rossmills
 *
 */

public class MapView {
	private AreaView[][] areaViews;
	
	// TODO See if these can be determined from the panel size - then we could have a resizable panel???
	// OR - different zoom levels?
	
	public MapView(GameMap map, ImageLoader tileLoader) {
		Area[][] areas = map.getAreaList();
		int cols = areas.length;
		int rows = areas[0].length;
		
		System.out.printf("MapView: %d areas created\n", cols*rows);
		
		areaViews = new AreaView[rows][cols];
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) {
				String imageType = areas[i][j].getPathType() + areas[i][j].getAreaType();
				int areaRotation = areas[i][j].getRotation();
				//System.out.printf("MapView: built url - %s %d\n", imageType, areaRotation);
				areaViews[i][j] = new AreaView( tileLoader, imageType, areaRotation, areas[i][j] );
			}
		}
		System.out.printf("MapView: areaViews.length=%d areaViews[0].length=%d\n", areaViews.length, areaViews[0].length);
	
	}
	
	public void draw(Graphics g) {
//		System.out.println("MapView: drawing");
		for(int i = 0; i < areaViews.length; i++) {
			for(int j = 0; j < areaViews[i].length; j++) {
				areaViews[i][j].draw(j * GameModel.TILE_WIDTH,
						i * GameModel.TILE_WIDTH,
						GameModel.TILE_WIDTH, 
						GameModel.TILE_HEIGHT,
						g);
			}
		}
	}
}
