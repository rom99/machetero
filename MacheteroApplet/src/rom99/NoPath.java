package rom99;

public class NoPath extends Area {
	
	public NoPath() {
		connects[NORTH] = false;
		connects[EAST] = false;
		connects[SOUTH] = false;
		connects[WEST] = false;
	}

	@Override
	public String getPathType() {
		return "Thick";
	}
	
	@Override
	public int getRotation() {
		return 0;
	}
}
