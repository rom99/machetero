/**
 * 
 */
package rom99;

import java.awt.Rectangle;

/**
 * @author rossmills
 *
 */
public class Snake extends Sprite {
	
	
	
	@Override
	public Rectangle getRectangle() {
		// A slightly smaller rectangle than standard ( by one third) 
		return new Rectangle(locX - ( getWidth()/3 ) , locY - getHeight()/3, getWidth()*2/3, getHeight()*2/3 );
	}

	/**
	 * Returns the human-readable name for this Sprite object
	 */
	@Override
	public String getName() {
		return "Snake";
	}
	
	@Override
	public int getWidth() {
		return 30;
	}
	
	@Override
	public int getHeight() {
		return 10;
	}

}
