/**
 * 
 */
package rom99;

import java.awt.Dimension;
import java.awt.Graphics;

/**
 * @author rossmills
 *
 */
public class SnakeView extends SpriteView {	
	
	public SnakeView(Sprite s, ImageLoader spriteLoader ) {
		this.sprite = s;
		imageLoader = spriteLoader;
		if ( !sprite.getName().equals("noImage") ) {			
			// System.out.printf("Fetching image: %s\n", s.getName() );
			image = imageLoader.getImage( s.getName() );			
		}		
	}
	
	/**
	 * Describes how to draw the snake
	 */ 
	@Override
	public void draw(Graphics g) {
		g.drawImage(image, 
				sprite.getLocX() - sprite.getWidth() / 2, 
				sprite.getLocY() - sprite.getHeight() / 2, 
				sprite.getWidth() , 
				sprite.getHeight(),
				null );
	}
	
	/**
	 * Returns the size this sprite should be rendered at
	 */
	@Override
	public Dimension getSpriteSize() {
		return new Dimension(sprite.getWidth(), sprite.getHeight() );
	}	
}
