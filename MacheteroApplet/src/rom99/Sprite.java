
package rom99;

import java.awt.Rectangle;


/**
 * Abstract class for all moving, animated sprites including Machetero and baddies.
 * @author rossmills
 *
 */
public abstract class Sprite {

	/*
	 * Absolute position on map
	 */
	protected int locX, locY;
	
	/*
	 * If this sprite is alive or not
	 */
	protected boolean alive = true;
	
	/*
	 * Direction facing (0 is NORTH, 1 is EAST, 2 is SOUTH, 3 is WEST)
	 */
	protected int direction;
	
	public void setDead() {
		alive = false;
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	/**
	 *  Sub-classes must provide a bounding box
	 */
	public Rectangle getRectangle() {
		return new Rectangle(locX - getWidth()/2, locY - getHeight()/2, getWidth(), getHeight() );
	}
	
	/**
	 *  Check if this sprite collides with another one
	 */
	public boolean checkHit(Sprite s) {
		return this.getRectangle().intersects(s.getRectangle() );
	}
	
	/**
	 * Sub-classes must provide a name by means of this method
	 * @return
	 */
	public abstract String getName();
	
	/**
	 * Width is supplied by this abstract method
	 * @return
	 */
	public abstract int getWidth();

	/**
	 * Height is supplied by this abstract method
	 * @return
	 */
	public abstract int getHeight();
	
	public int getRow() {
		return locY / GameModel.TILE_HEIGHT;
	}
	
	public int getCol() {
		return locX / GameModel.TILE_WIDTH;
	}
		
	/**
	 * Move the sprite on the X axis (left to right or vice versa)
	 * @param move
	 */
	public void moveX(int move) {
		locX += move;
		//System.out.println("Sprite: locX="+locX);
	}
	
	/**
	 * Move the sprite on the Y axis (right to left, or vice versa)
	 * @param move
	 */
	public void moveY(int move) {
		locY += move;
		//System.out.println("Sprite: locY="+locY);
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getLocX() {
		return locX;
	}

	public void setLocX(int locX) {
		this.locX = locX;
	}

	public int getLocY() {
		return locY;
	}

	public void setLocY(int locY) {
		this.locY = locY;
	}

	// Further attributes such as health, inventory, etc can be added later
}
