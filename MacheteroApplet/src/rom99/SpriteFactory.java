package rom99;

import java.util.ArrayList;
import java.util.Random;

public class SpriteFactory {
	
	GameMap map;
	
	public SpriteFactory(GameMap map) {
		this.map = map;
	}
	
	public ArrayList<Sprite> getRandomSprites() {
		int mapRowSize = map.getAreaList().length;
		int mapColSize = map.getAreaList()[0].length;
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		
		// Put one at the centre of each tile as a test
		for(int i = 0; i < mapRowSize; i++) {
			for(int j = 0; j < mapColSize; j++) {
				Random random = new Random();
				int r = random.nextInt(4);
				// Randomly select tiles for snakes, but not the start tile
				if(r == 0 && !(i == map.getStartRow() && j == map.getStartCol() ) ) {
					Sprite s = new Snake();
					s.setLocX( j * GameModel.TILE_WIDTH + GameModel.TILE_WIDTH/2 );
					s.setLocY( i * GameModel.TILE_HEIGHT + GameModel.TILE_HEIGHT/2);

//					System.out.printf("SpriteFactory: new Snake at (%d, %d), row=%d, col=%d\n",
//							s.getLocX(), s.getLocY(), 
//							s.getLocY() / GameModel.TILE_HEIGHT,
//							s.getLocX() / GameModel.TILE_WIDTH);
					sprites.add( s );
				}
			}
		}
		return sprites;
	}
}
