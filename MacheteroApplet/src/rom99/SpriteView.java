package rom99;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * Abstract class describing how to render a specific sprite
 * @author rossmills
 *
 */
public abstract class SpriteView {
	
	protected Sprite sprite;
	protected BufferedImage image;
	protected ImageLoader imageLoader;
		
	/**
	 * Sub-classes of SpriteView must implement a draw method. They can access their associated 
	 * Sprite object - i.e. sprite.getLocX(), etc.
	 * @param g
	 */
	public abstract void draw(Graphics g);
	
	public abstract Dimension getSpriteSize();

	public Sprite getSprite() {
		return sprite;
	}
}
