package rom99;

public class StraightPath extends Area {
	
	public StraightPath() {
		connects[NORTH] = false;
		connects[EAST] = true;
		connects[SOUTH] = false;
		connects[WEST] = true;
	}

	@Override
	public String getPathType() {
		return "Straight";
	}
	
	@Override
	public int getRotation() {
		return rotation % 2;
	}
}
