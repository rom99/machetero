package rom99;

public class TeePath extends Area {

	public TeePath() {
		connects[NORTH] = true;
		connects[EAST] = true;
		connects[SOUTH] = false;
		connects[WEST] = true;
	}

	@Override
	public String getPathType() {
		return "Tee";
	}

}
