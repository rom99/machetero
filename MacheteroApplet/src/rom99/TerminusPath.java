package rom99;

public class TerminusPath extends Area {

	public TerminusPath() {
		connects[NORTH] = true;
		connects[EAST] = false;
		connects[SOUTH] = false;
		connects[WEST] = false;
	}

	@Override
	public String getPathType() {
		return "Terminus";
	}
}
