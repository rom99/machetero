# Machetero
A tile-based game involving a hat-wearing jungle wanderer, hacking his way through the greenery and shooting any snakes that cross his path. The snakes bite back if you get too close.

It's a Java Applet but was originally developed as a desktop application. It no longer seems to run in the latest Chrome or Safari browsers so maybe I should take the time to switch it back to a desktop application. It does however run as a Java applet when launched from within the Eclipse IDE. 

This was a very fun project, and my first attempt at making a game. The maps are procedurally generated, and will always guarantee a path from the bottom of the playing area to the top. The game is won by reaching the top of the playing area.  A score is given based on health remaining, time taken and snakes killed.

The name Machetero was chosen to mean "someone wielding a machete", and does not refer to any group of terrorists, freedom-fighters, or any other organisation.